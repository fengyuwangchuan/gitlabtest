package com.atguigu.jxc.controller;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class DamageListGoodsController {
    @Autowired
    DamageListGoodsService damageListGoodsService;
    @PostMapping("/damageListGoods/list")
    public Map<String,Object> damageListGoodsList(@RequestParam(value = "sTime",required = false)String  sTime,
                                                  @RequestParam(value = "eTime",required = false)String  eTime,
                                                  HttpSession httpSession){
        Map<String,Object> map = damageListGoodsService.damageListGoodsList(sTime,eTime,httpSession);
        return map;

    }

    @PostMapping("/damageListGoods/goodsList")
    public Map<String,Object> damageListGoodsDetail(@RequestParam("damageListId") Integer damageListId,
                                                    HttpSession session){
        Map<String,Object> map = damageListGoodsService.search(damageListId,session);
        return map;
    }

}
