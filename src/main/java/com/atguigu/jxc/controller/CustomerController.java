package com.atguigu.jxc.controller;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @PostMapping("/customer/list")
    public Map<String,Object> getCustomerList(@RequestParam("page") Integer page,
                                              @RequestParam("rows") Integer rows,
                                              @RequestParam(value = "customerName", required = false) String  customerName){
        Map<String,Object> map = customerService.getCustomerList(page,rows,customerName);
        return map;
    }

    @PostMapping("/customer/save")
    public Result saveOrUpdate(@RequestParam(value = "customerId",required = false) Long customerId,
                               Customer customer){
        customerService.saveOrUpdate(customerId,customer);
        return Result.ok();

    }

    @PostMapping("/customer/delete")
    public Result deleteByids(@RequestParam("ids") String ids){
        customerService.deleteByids(ids);
        return Result.ok();
    }
}
