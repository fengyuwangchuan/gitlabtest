package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverFlowService;
import com.atguigu.jxc.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class OverFlowController {
    @Autowired
    OverFlowService overFlowService;

    @PostMapping("/overflowListGoods/save")
    public Result  saveOverflowListGoods(@RequestParam("overflowNumber") String overflowNumber,
                                         @RequestParam("overflowListGoodsStr")String overflowListGoodsStr,
                                         OverflowList overflowList, HttpSession httpSession
                                         ){
        overFlowService.saveOverflowListGoods(overflowNumber,overflowListGoodsStr,overflowList,httpSession);
        return Result.ok();
    }

    @PostMapping("/overflowListGoods/list")
    public Map<String,Object> list(@RequestParam("sTime") String  sTime,
                                   @RequestParam("eTime") String  eTime,
                                   HttpSession session){
        Map<String,Object> map = overFlowService.list(sTime,eTime,session);
        return map;
    }
    @PostMapping("/overflowListGoods/goodsList")
    public Map<String,Object> goodsList(@RequestParam("overflowListId") Integer overflowListId){
        Map<String,Object>map = overFlowService.goodsList(overflowListId);
        return map;
    }
}
