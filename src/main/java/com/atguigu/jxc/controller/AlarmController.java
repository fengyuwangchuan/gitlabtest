package com.atguigu.jxc.controller;

import com.atguigu.jxc.service.AlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class AlarmController {
    @Autowired
    AlarmService alarmService;
    @PostMapping("/goods/listAlarm")
    public Map<String,Object> listAlarm(){
        Map<String,Object> map = alarmService.listAlarm();
        return map;
    }


}
