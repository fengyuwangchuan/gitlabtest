package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.UserService;
import com.atguigu.jxc.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class DamageController {
    @Autowired
    DamageService damageService;
    @Autowired
    UserService userService;

    @PostMapping("/damageListGoods/save")
    public Result saveDamageListGoods(@RequestParam("damageNumber")String damageNumber,
                                      @RequestParam("damageListGoodsStr") String damageListGoodsStr,
                                      DamageList damageList,
                                      HttpSession httpSession){

        damageService.saveDamageListGoods(damageNumber,damageList,damageListGoodsStr,httpSession);
        return Result.ok();
    }
}
