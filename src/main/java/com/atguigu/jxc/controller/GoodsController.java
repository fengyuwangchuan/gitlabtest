package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.util.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@Transactional
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param codeOrName 商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/goods/listInventory")
    public Map<String, Object>  getlistInventoryVo(@RequestParam("page") Integer page,
                                                   @RequestParam("rows") Integer rows,
                                                   @RequestParam(value = "codeOrName",required = false) String codeOrName,
                                                   @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId){
        Map<String, Object> map = goodsService.getlistInventoryVo(page, rows, codeOrName, goodsTypeId);
        return map;
    }

    @PostMapping("/goods/list")
    public Map<String, Object>  getList(@RequestParam("page") Integer page,
                                        @RequestParam("rows") Integer rows,
                                        @RequestParam(value = "goodsName",required = false) String goodsName,
                                        @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId){
        Map<String, Object> map = goodsService.getList(page, rows, goodsName, goodsTypeId);
        return map;
    }

    @PostMapping("/goodsType/save")
    public Result addType(String goodsTypeName, Integer pId){
        goodsService.addType(goodsTypeName,pId);
        return  Result.ok();
    }

    @PostMapping("/goodsType/delete")
    public Result deleteGoodsType(@RequestParam("goodsTypeId")Integer  goodsTypeId){
        goodsService.deleteType(goodsTypeId);
        return Result.ok();
    }

    @PostMapping("/goods/save")
    public Result saveOrUpdate(@RequestParam(value = "goodsId",required = false)Integer goodsId,
                               Goods goods){
        goodsService.saveOrUpdate(goodsId,goods);
        return Result.ok();
    }

    @PostMapping("/goods/delete")
    public Result deleteGood(@RequestParam("goodsId")Integer goodsId){
        goodsService.deleteGood(goodsId);
        return Result.ok();
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     * @param goods 商品信息实体
     * @return
     */

    /**
     * 删除商品信息
     * @param goodsId 商品ID
     * @return
     */

    /**
     * 分页查询无库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryQuantity(@RequestParam("page") Integer page,
                                                     @RequestParam("rows") Integer rows,
                                                     @RequestParam(value = "nameOrCode",required = false)String nameOrCode){
        Map<String,Object> map = goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
        return map;
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/goods/getHasInventoryQuantity")
    public Map<String ,Object> getHasInventoryQuantity(@RequestParam("page") Integer page,
                                                       @RequestParam("rows")Integer rows,
                                                       @RequestParam(value = "nameOrCode",required = false)String nameOrCode){
        Map<String,Object> map = goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
        return map;

    }

    /**
     * 添加商品期初库存
     * @param goodsId 商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice 成本价
     * @return
     */
    @PostMapping("/goods/saveStock")
    public Result saveStock(@RequestParam(value = "goodsId") Integer goodsId,
                     @RequestParam("inventoryQuantity") Integer inventoryQuantity,
                     @RequestParam("purchasingPrice")double purchasingPrice){
        goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return Result.ok();
    }
    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/goods/deleteStock")
    public ServiceVO deleteStock(@RequestParam("goodsId") Integer goodsId){
        Long stat = goodsService.getStockStat(goodsId);
        if (stat==0){
        goodsService.deleteStock(goodsId);
        return new ServiceVO(100,"请求成功");
        }else {

       return new ServiceVO(401,"该商品有库存或已入库,不能删除");
        }
    }

    /**
     * 查询库存报警商品信息
     * @return
     */

}
