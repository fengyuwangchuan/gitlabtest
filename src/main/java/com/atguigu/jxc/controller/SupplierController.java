package com.atguigu.jxc.controller;

import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    /**
     *
     * @param page 当前页数
     * @param rows 每页显示的记录数
     * @param supplierName 供应商名字
     * @return
     */
    @PostMapping("/supplier/list")
    public Map<String,Object> getSupplier(Integer page, Integer rows, String supplierName){
        Map<String,Object> map = supplierService.getSupplier(page,rows,supplierName);
        return map;
    }

    /**
     * 增加或保存
     * @param supplierId
     * @param supplier
     * @return
     */
    @PostMapping("/supplier/save")
    public Result saveOrUpdate(@RequestParam(value = "supplierId",required = false)Integer supplierId,
                               Supplier supplier){
        supplierService.saveOrUpdate(supplierId,supplier);
        return Result.ok();
    }

    /**
     * (支持批量)删除
     * @param ids
     * @return
     */
    @PostMapping("/supplier/delete")
    public Result deleteById(String ids){
        supplierService.deleteById(ids);
        return Result.ok();
    }


}
