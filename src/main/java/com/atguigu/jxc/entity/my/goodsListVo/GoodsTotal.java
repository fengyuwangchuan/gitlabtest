package com.atguigu.jxc.entity.my.goodsListVo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GoodsTotal {
    Long goodsId;
    Integer saleNum;
    Integer returnNum;
}
