package com.atguigu.jxc.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public  class Result<T> {
    private Integer code;
    private String msg;
    private T info;

    public static <T> Result<T> ok(){
        Result<T> result = new Result<>();
        result.setCode(100);
        result.setMsg("请求成功");
        return  result;
    }

    public static  <T> Result<T> ok(T data){
        Result<T> result = new Result<>();
        result.setCode(100);
        result.setMsg("请求成功");
        result.setInfo(data);
        return  result;
    }
}
