package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OverFlowDao {
    void saveList(@Param("overflowList") OverflowList overflowList);

    void savegood(@Param("overflowListGoods") OverflowListGoods overflowListGoods);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
