package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.my.goodsListVo.GoodsTotal;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
@Mapper
public interface GoodsDao {


    String getMaxCode();


    List<Goods> getlistInventoryVo(@Param("pageBegin") Integer pageBegin,
                             @Param("rows") Integer rows,
                             @Param("codeOrName") String codeOrName,
                             @Param("goodsTypeId") Integer goodsTypeId);


    Long getTotal(@Param("codeOrName") String codeOrName,
                  @Param("goodsTypeId") Integer goodsTypeId);

    GoodsTotal getCount(@Param("goodsId") Integer goodsId);

    List<Goods> getList(@Param("pageBegin") Integer pageBegin,
                        @Param("rows") Integer rows,
                        @Param("goodsName") String goodsName,
                        @Param("goodsTypeId") Integer goodsTypeId);

    Long getTotall(@Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    void addType(@Param("goodsTypeName") String goodsTypeName, @Param("pId") Integer pId);

    Long getTypeStat(@Param("pId") Integer pId);

    void setTypeStat(@Param("pId") Integer pId, @Param("i") int i);

    void save(@Param("goods") Goods goods);

    void updateById(@Param("goodsId") Integer goodsId, @Param("goods") Goods goods);

    void deleteType(@Param("goodsTypeId") Integer goodsTypeId);

    List<GoodsType> getTreeType(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType getTypebyId(@Param("goodsTypeId") Integer goodsTypeId);

    void deleteGood(@Param("goodsId") Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("pageBegin") Integer pageBegin,
                                       @Param("rows") Integer rows,
                                       @Param("nameOrCode") String nameOrCode);

    Long getNoInventoryQuantityTotal(@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("pageBegin") Integer pageBegin, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Long gethasInventoryQuantityTotal(@Param("nameOrCode") String nameOrCode);

    void updateStock(@Param("goodsId") Integer goodsId,
                     @Param("inventoryQuantity") Integer inventoryQuantity,
                     @Param("purchasingPrice") double purchasingPrice);


    void deleteStock(@Param("goodsId") Integer goodsId);

    void setStockStat(@Param("goodsId") Integer goodsId, @Param("i") int i);

    Long getStockStat(@Param("goodsId") Integer goodsId);
}
