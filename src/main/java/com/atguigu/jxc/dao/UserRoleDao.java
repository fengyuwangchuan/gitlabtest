package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description 用户角色
 */
@Mapper
public interface UserRoleDao {

    // 根据用户id删除用户角色
    Integer deleteUserRoleByUserId(Integer userId);

    // 为用户添加角色
    Integer addUserRole(UserRole userRole);
}
