package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper

public interface SupplierDao {
    /**
     * 查供应商
     * @param pageBegin
     * @param rows
     * @param supplierName
     * @return
     */
    List<Supplier> getSupplier(@Param("pageBegin") Integer pageBegin,
                               @Param("rows") Integer rows,
                               @Param("supplierName") String supplierName);

    /**
     * 查询条件对应的供应商总条数
     * @param supplierName
     * @return
     */
    Long getCount(@Param("supplierName") String supplierName);

    void save(@Param("supplier") Supplier supplier);

    void updateById(@Param("supplierId") Integer supplierId, @Param("supplier") Supplier supplier);




    void deleteBaych(@Param("split") String[] split);
}
