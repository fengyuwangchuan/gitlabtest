package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DamageListGoodsDao {
    void save(@Param("damageListGoods") DamageListGoods damageListGoods);

    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> search(@Param("damageListId") Integer damageListId);
}
