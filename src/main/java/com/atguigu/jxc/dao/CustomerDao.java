package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerDao {
    List<Customer> getCustomerList(@Param("pageBegin") Integer pageBegin,
                                   @Param("rows") Integer rows,
                                   @Param("customerName") String customerName);

    Integer getCount(@Param("customerName") String customerName);

    void updateById(@Param("customerId") Long customerId, @Param("customer") Customer customer);

    void save(@Param("customer") Customer customer);

    void deleteByids(@Param("split") String[] split);
}
