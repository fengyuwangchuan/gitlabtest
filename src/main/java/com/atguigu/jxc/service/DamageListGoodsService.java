package com.atguigu.jxc.service;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListGoodsService {
    Map<String, Object> damageListGoodsList(String sTime, String eTime, HttpSession httpSession);

    Map<String, Object> search(Integer damageListId,HttpSession session);
}
