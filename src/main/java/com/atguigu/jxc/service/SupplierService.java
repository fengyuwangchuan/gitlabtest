package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName);


    void saveOrUpdate(Integer supplierId, Supplier supplier);

    void deleteById(String ids);
}
