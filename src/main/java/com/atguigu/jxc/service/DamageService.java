package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;

public interface DamageService {

    void saveDamageListGoods(String damageNumber, DamageList damageList, String damageListGoodsStr,HttpSession httpSession);
}
