package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverFlowService {
    void saveOverflowListGoods(String overflowNumber,
                               String overflowListGoodsStr,
                               OverflowList overflowList,
                               HttpSession httpSession);

    Map<String, Object> list(String sTime, String eTime,HttpSession session);

    Map<String, Object> goodsList(Integer overflowListId);
}
