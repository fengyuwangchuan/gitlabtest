package com.atguigu.jxc.service;

import java.util.Map;

public interface AlarmService {
    Map<String, Object> listAlarm();
}
