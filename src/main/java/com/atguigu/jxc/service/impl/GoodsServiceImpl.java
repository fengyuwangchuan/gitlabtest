package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.my.goodsListVo.GoodsTotal;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getlistInventoryVo(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
//        数据起始位置
        Integer pageBegin = (page-1)*rows;
//        基本属性
        List<Goods> goods = goodsDao.getlistInventoryVo(pageBegin, rows, codeOrName, goodsTypeId);
//        total总数
        Long totalNum = goodsDao.getTotal(codeOrName, goodsTypeId);

//        销量封装
        for (Goods good : goods) {
        GoodsTotal total = goodsDao.getCount(good.getGoodsId());
//        买
            Integer saleNum = total.getSaleNum();
            saleNum = saleNum==null?0:saleNum;
//        退
            Integer returnNum = total.getReturnNum();
            returnNum=returnNum==null?0:returnNum;
            Integer t=saleNum-returnNum<0?0:saleNum-returnNum;
            good.setSaleTotal(t);
        }

//封装数据
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",totalNum);
        map.put("rows",goods);
        return map;
    }

    @Transactional
    @Override
    public Map<String, Object> getList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Integer pageBegin = (page-1)*rows;
//        基本信息
        List<Goods> list = goodsDao.getList(pageBegin,rows,goodsName,goodsTypeId);
//        总数
        Long total = goodsDao.getTotall(goodsName,goodsTypeId);
        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("rows",list);
        return  map;
    }

    @Override
    public void addType(String goodsTypeName, Integer pId) {
        Long pStat = goodsDao.getTypeStat(pId);
        if (pStat==0){
            goodsDao.setTypeStat(pId,1);
        }
        goodsDao.addType(goodsTypeName,pId);
    }
    @Override
    public void deleteType(Integer goodsTypeId) {
        GoodsType goodsType = goodsDao.getTypebyId(goodsTypeId);
        Integer pId = goodsType.getPId();
        Long typeStat = goodsDao.getTypeStat(goodsTypeId);
        //        判断删除本类后,父类是否会变成叶子结点
        if (goodsType.getPId()!=null){
            List<GoodsType> treeType = goodsDao.getTreeType(pId);
//        变成叶子结点以后要修改状态
            if (treeType.size()==1){
                goodsDao.setTypeStat(pId,0);
            }
        }
//        判断删除的分类是否是叶子结点
//        不是叶子结点
        if (typeStat!=0){
            List<GoodsType> list = goodsDao.getTreeType(goodsTypeId);
            for (GoodsType goodsType1 : list) {
                goodsDao.deleteType(goodsType1.getGoodsTypeId());
            }
        goodsDao.deleteType(goodsTypeId);
        }else {
//            是叶子结点
        goodsDao.deleteType(goodsTypeId);
        }

    }

    @Override
    public void deleteGood(Integer goodsId) {
        goodsDao.deleteGood(goodsId);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page,
                                                      Integer rows,
                                                      String nameOrCode) {
        Integer pageBegin = (page-1)*rows;
//        查数据
        List<Goods>goods = goodsDao.getNoInventoryQuantity(pageBegin,rows,nameOrCode);
//        查条数
        Long total = goodsDao.getNoInventoryQuantityTotal(nameOrCode);
        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("rows",goods);
        return map;

    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Integer pageBegin = (page-1)*rows;
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(pageBegin,rows,nameOrCode);
        Long num = goodsDao.gethasInventoryQuantityTotal(nameOrCode);
        HashMap<String, Object> map = new HashMap<>();
        map.put("total",num);
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        goodsDao.updateStock(goodsId,inventoryQuantity,purchasingPrice);

    }

    @Override
    public void deleteStock(Integer goodsId) {
        goodsDao.deleteStock(goodsId);
    }

    @Override
    public Long getStockStat(Integer goodsId) {
       return goodsDao.getStockStat(goodsId);
    }

    @Override
    public void saveOrUpdate(Integer goodsId, Goods goods) {
        if (goodsId==null) {
            goods.setGoodsCode(this.getCode().getInfo().toString());
            goodsDao.save(goods);
        }else {
            goodsDao.updateById(goodsId,goods);
        }
    }



}
