package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.AlarmDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.AlarmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AlarmServiceImpl implements AlarmService {
    @Autowired
    AlarmDao alarmDao;
    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> list = alarmDao.listAlarm();
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
