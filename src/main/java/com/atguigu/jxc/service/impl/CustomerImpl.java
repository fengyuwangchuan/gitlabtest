package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerImpl implements CustomerService {
    @Autowired
    CustomerDao customerDao;
    @Override
    public Map<String, Object> getCustomerList(Integer page,
                                               Integer rows,
                                               String customerName) {
//        查基础数据
        Integer pageBegin=0;
        if (page>=1){
        pageBegin=(page-1)*rows;
        }
        List<Customer> list = customerDao.getCustomerList(pageBegin,rows,customerName);
//        查数据条数
        Integer total = customerDao.getCount(customerName);

//        封装数据
        HashMap<String, Object> map = new HashMap<>();
        map.put("toatal",total);
        map.put("rows",list);
        return map;
    }

    @Override
    public void saveOrUpdate(Long customerId, Customer customer) {
        if (customerId!=null){
        customerDao.updateById(customerId,customer);
        }else {
        customerDao.save(customer);
        }
    }

    @Override
    public void deleteByids(String ids) {
        String[] split = ids.split(",");
        customerDao.deleteByids(split);
    }
}
