package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverFlowDao;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverFlowService;
import com.atguigu.jxc.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverFlowServiceImpl implements OverFlowService {
    @Autowired
    OverFlowDao overFlowDao;
    @Autowired
    UserService userService;
    @Override
    public void saveOverflowListGoods(String overflowNumber,
                                      String overflowListGoodsStr,
                                      OverflowList overflowList,
                                      HttpSession httpSession) {
        overflowList.setOverflowNumber(overflowNumber);
        User user = (User)httpSession.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());
        overFlowDao.saveList(overflowList);
        Gson gson = new Gson();
        List<OverflowListGoods> list =(List<OverflowListGoods>) gson.fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
        }.getType());
        for (OverflowListGoods overflowListGoods : list) {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overFlowDao.savegood(overflowListGoods);
        }

    }

    @Override
    public Map<String, Object> list(String sTime, String eTime,HttpSession session) {
        User user = (User)session.getAttribute("currentUser");
        List<OverflowList>list = overFlowDao.list(sTime,eTime);
        for (OverflowList overflowList : list) {
            overflowList.setTrueName(user.getTrueName());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        List<OverflowListGoods> list = overFlowDao.goodsList(overflowListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;

    }
}
