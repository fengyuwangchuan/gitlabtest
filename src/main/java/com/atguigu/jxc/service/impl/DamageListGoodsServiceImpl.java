package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    DamageListGoodsDao damageListGoodsDao;
    @Autowired
    UserService userService;
    @Override
    public Map<String, Object> damageListGoodsList(String sTime, String eTime, HttpSession httpSession) {
        User user = (User)httpSession.getAttribute("currentUser");

        List<DamageList> list = damageListGoodsDao.list(sTime,eTime);
        for (DamageList damageList : list) {
            damageList.setTrueName(user.getTrueName());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }

    @Override
    public Map<String, Object> search(Integer damageListId,HttpSession session) {

        List<DamageListGoods>list = damageListGoodsDao.search(damageListId);
        Map<String, Object> map = new HashMap<>();
        map.put("rows",list);
        return map;
    }
}
