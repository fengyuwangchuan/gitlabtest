package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atguigu.jxc.service.DamageService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Service

public class DamageServiceImpl implements DamageService {
    @Autowired
    GoodsService goodsService;
    @Autowired
    DamageDao damageDao;
    @Autowired
    DamageListGoodsDao damageListGoodsDao;
    @Autowired
    UserService userService;



    @Override
    public void saveDamageListGoods(String damageNumber, DamageList damageList, String damageListGoodsStr,HttpSession httpSession) {
        damageList.setDamageNumber(damageNumber);
        Map<String, Object> stringObjectMap = userService.loadUserInfo(httpSession);
        String userName =(String) stringObjectMap.get("userName");
        User user = userService.getByname(userName);
        damageList.setUserId(user.getUserId());
        damageList.setTrueName(user.getTrueName());
        damageDao.saveDamageList(damageList);
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.save(damageListGoods);
        }
    }
}
