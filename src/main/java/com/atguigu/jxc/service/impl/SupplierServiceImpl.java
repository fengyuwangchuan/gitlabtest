package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atguigu.jxc.service.SupplierService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    SupplierDao supplierDao;

    /**
     * 查供应商
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName) {
        Integer pageBegin = (page-1)*rows;
//        查基本数据
        List<Supplier> supplierList = supplierDao.getSupplier(pageBegin,rows,supplierName);
//        封装数据
        HashMap<String, Object> map = new HashMap<>();
        Long totalNum = supplierDao.getCount(supplierName);
//        查总条数
        map.put("total",totalNum);
        map.put("rows",supplierList);
        return map;
    }

    @Override
    public void saveOrUpdate(Integer supplierId, Supplier supplier) {
        if (supplierId==null){
            supplierDao.save(supplier);
        }else {
            supplierDao.updateById(supplierId,supplier);
        }
    }

    @Override
    public void deleteById(String ids) {
        String[] split = ids.split(",");
        supplierDao.deleteBaych(split);
/*        for (String s : split) {
            int i = Integer.parseInt(s);
            supplierDao.deleteById(i);
        }*/
    }


}
