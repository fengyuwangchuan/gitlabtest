package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> getCustomerList(Integer page, Integer rows, String customerName);

    void saveOrUpdate(Long customerId, Customer customer);

    void deleteByids(String ids);
}
